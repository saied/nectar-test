//
//  NectarListView.swift
//  NectarTest
//
//  Created by Saied Nawaz on 27/10/2020.
//

import SwiftUI

struct NectarListView: View {

    let nectars: [NectarViewModel]

    var body: some View {
        //TODO group by date
        List(self.nectars, id: \.transactionId) { nectar in
            NavigationLink(destination: NectarDetailView(nectar: nectar)) {
            NectarCellView(nectar: nectar)
            }
        }
    }
}

struct NectarListView_Previews: PreviewProvider {
    static var previews: some View {

        let item = Item(
                transactionDate: ISO8601DateFormatter().date(from: "2010-01-01T12:00:00+0000"),
                processedDate: ISO8601DateFormatter().date(from: "2010-01-01T12:00:00+0000"),
                transactionId: "12312312",
                headerText: "Fake Header",
                descriptionText: "Fake description",
                lineItems: [],
                points: "+1",
                monetaryValue: "1.6",
                displayType: displayType.normal,
                detailTexts: ["You have more fake text"])

        NectarListView(nectars: [NectarViewModel(nectar: item)])
    }
}
