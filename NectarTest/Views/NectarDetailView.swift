//
//  NectarDetailView.swift
//  NectarTest
//
//  Created by Saied Nawaz on 27/10/2020.
//

import SwiftUI

struct NectarDetailView: View {
    let nectar: NectarViewModel

    var body: some View {
        VStack {
            ZStack {
                Color(red: 121 / 255, green: 53 / 255, blue: 238 / 255)

                VStack(alignment: .leading) {
                    Text("27 June 2019").font(.title)

                    HStack(alignment: .top) {

                        VStack(alignment: .leading) {
                            Text(nectar.title).font(.title3)
                            Text(nectar.description).font(.subheadline)
                        }

                        Spacer()
                        Text(nectar.points).font(.title3)
                    }
                }.padding()
    
            } .frame(minWidth: 0,
                     maxWidth: .infinity,
                     minHeight: 0,
                     maxHeight: 100)
            
            List(nectar.lineItems, id: \.description) { lineItem in
                HStack {
                    //TODO should really be in a viewmodel
                    Text(lineItem.description ?? "No Data")
                    Spacer()
                    Text(lineItem.points ?? "No Data").fontWeight(.bold)
                }
            }
        }

    }
}

struct NectarDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let lineItem = LineItem(description: "Cheese on toast", points: "+25", artworkUrl: "")

        let item = Item(
                transactionDate: ISO8601DateFormatter().date(from: "2010-01-01T12:00:00+0000"),
                processedDate: ISO8601DateFormatter().date(from: "2010-01-01T12:00:00+0000"),
                transactionId: "12312312",
                headerText: "Fake Header",
                descriptionText: "Fake description",
                lineItems: [lineItem],
                points: "+1",
                monetaryValue: "1.6",
                displayType: displayType.normal,
                detailTexts: ["You have more fake text"])
        NectarDetailView(nectar: NectarViewModel(nectar: item))
    }
}
