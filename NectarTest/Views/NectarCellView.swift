//
//  NectarCellView.swift
//  NectarTest
//
//  Created by Saied Nawaz on 27/10/2020.
//

import SwiftUI

struct NectarCellView: View {
    let nectar: NectarViewModel

    var body: some View {
        return HStack {
            VStack(alignment: .leading) {
                Text(nectar.title)
                        .fontWeight(.bold)
                        .font(.title3)
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 0))
                Text(nectar.description)
                        .fontWeight(.light)
            }
            Spacer()
            VStack {
                Text(nectar.points)
                        .fontWeight(.bold)
            }
        }
    }
}

struct NectarCellView_Previews: PreviewProvider {
    static var previews: some View {
        let item = Item(
                transactionDate: ISO8601DateFormatter().date(from: "2010-01-01T12:00:00+0000"),
                processedDate: ISO8601DateFormatter().date(from: "2010-01-01T12:00:00+0000"),
                transactionId: "12312312",
                headerText: "Fake Header",
                descriptionText: "Fake description",
                lineItems: [],
                points: "+1",
                monetaryValue: "1.6",
                displayType: displayType.normal,
                detailTexts: ["You have more fake text"])

        NectarCellView(nectar: NectarViewModel(nectar: item))
    }
}
