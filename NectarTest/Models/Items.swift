//
//  Items.swift
//  NectarTest
//
//  Created by Saied Nawaz on 27/10/2020.
//

import Foundation

enum displayType: String, CaseIterable, Codable {
    case normal = "NORMAL"
    case bonus = "BONUS"
}

struct Items: Decodable {
    let limit: Int?
    let pageSize: Int?
    let nextToken: String?
    let totalTransactions: Int?
    let transactionFeedStatus: String?
    let items: [Item]?
}

struct Item: Decodable {
    let transactionDate: Date?
    let processedDate: Date?
    let transactionId: String?
    let headerText: String?
    let descriptionText: String?
    let lineItems: [LineItem]?
    let points: String?
    let monetaryValue: String?
    let displayType: displayType?
    let detailTexts: [String]?
}

struct LineItem: Decodable {
    let description: String?
    let points: String?
    let artworkUrl: String?
}
