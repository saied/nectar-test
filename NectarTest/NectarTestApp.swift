//
//  NectarTestApp.swift
//  NectarTest
//
//  Created by Saied Nawaz on 27/10/2020.
//

import SwiftUI

@main
struct NectarTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
