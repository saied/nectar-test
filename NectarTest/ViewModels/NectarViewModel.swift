//
//  NectarViewModel.swift
//  NectarTest
//
//  Created by Saied Nawaz on 27/10/2020.
//

import Foundation

struct NectarViewModel {
    let nectar: Item

    var title: String {
        return nectar.headerText ?? "No data"
    }

    var description: String {
        return nectar.descriptionText ?? "No data"
    }

    var points: String {
        return nectar.points ?? "No data"
    }

    var transactionId: String {
        return nectar.transactionId ?? "No data"
    }
    
    var lineItems: [LineItem] {
        return nectar.lineItems ?? []
    }
}
