//
//  NectarListViewModel.swift
//  NectarTest
//
//  Created by Saied Nawaz on 27/10/2020.
//

import Foundation

class NectarListViewModel: ObservableObject {
    @Published var nectars: [NectarViewModel] = [NectarViewModel]()

    func load() {
        fetchNectars()
    }

    private func fetchNectars() {
//        Webservice().getNectarLists { nectars in
//            if let nectars = nectars?.items {
//                DispatchQueue.main.async {
//                    self.nectars = nectars.map(NectarViewModel.init)
//                }
//            }
//        }

        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "example", withExtension: ".json") else {
            fatalError("Could not load example.json")
        }

        guard let jsonData = try? Data(contentsOf: url) else {
            fatalError("Could not load data from json")
        }
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        let decodedJson = try? decoder.decode(Items.self, from: jsonData)
        //TODO would be nice to know why it fails
        
        if let nectars = decodedJson?.items {
            self.nectars = nectars.map(NectarViewModel.init)
        }
    }

}
