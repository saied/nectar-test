//
//  Webservice.swift
//  NectarTest
//
//  Created by Saied Nawaz on 27/10/2020.
//

import Foundation

class Webservice {

    func getNectarLists(completion: @escaping (Items?) -> Void) {
        guard let url = URL(string: "") else {
            fatalError("Could not fetch URL")
        }

        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data, error == nil else {
                completion(nil)
                return
            }

            let nectars = try? JSONDecoder().decode(Items.self, from: data)
            nectars == nil ? completion(nil) : completion(nectars)

        }.resume()
    }
}
