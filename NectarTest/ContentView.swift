//
//  ContentView.swift
//  NectarTest
//
//  Created by Saied Nawaz on 27/10/2020.
//

import SwiftUI

struct ContentView: View {

    @ObservedObject private var nectarListVM = NectarListViewModel()

    init() {
        UINavigationBar.appearance().barTintColor = UIColor(displayP3Red: 121/255, green: 53/255, blue: 238/255, alpha: 1.0)
        UINavigationBar.appearance().backgroundColor = UIColor(displayP3Red: 121/255, green: 53/255, blue: 238/255, alpha: 1.0)
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white]
        nectarListVM.load()
    }

    var body: some View {
        NavigationView {
            ZStack {
                NectarListView(nectars: self.nectarListVM.nectars)
            }
                    .navigationBarTitle("Activity", displayMode: .inline)
        }.edgesIgnoringSafeArea(Edge.Set(.bottom))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
