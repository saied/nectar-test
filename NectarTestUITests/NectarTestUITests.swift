//
//  NectarTestUITests.swift
//  NectarTestUITests
//
//  Created by Saied Nawaz on 27/10/2020.
//

import XCTest

class NectarTestUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        
        let tablesQuery = app.tables
        
        //TODO make this less brittle
        tablesQuery/*@START_MENU_TOKEN@*/.buttons["Sainsbury's ♥️\n+1\nFetter Lane Local"]/*[[".cells[\"Sainsbury's ♥️\\n+1\\nFetter Lane Local\"].buttons[\"Sainsbury's ♥️\\n+1\\nFetter Lane Local\"]",".buttons[\"Sainsbury's ♥️\\n+1\\nFetter Lane Local\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssert(app.staticTexts["Sainsbury's ♥️"].exists)
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
