//
//  NectarTestTests.swift
//  NectarTestTests
//
//  Created by Saied Nawaz on 27/10/2020.
//

import XCTest
@testable import NectarTest

class NectarTestTests: XCTestCase {

    var jsonData: Data?
    var decodedJson: Items?

    override func setUpWithError() throws {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "sample", withExtension: ".json") else {
            XCTFail("Could not find the file sample.json")
            return
        }
        jsonData = try Data(contentsOf: url)

        guard let jsonData = jsonData else {
            XCTFail("Could not get jsonData")
            return
        }
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        XCTAssertNoThrow(decodedJson = try decoder.decode(Items.self, from: jsonData), "Could not decode Sample json")
    }

    func test_items_has_correct_value() throws {
        XCTAssertEqual(decodedJson?.limit, 20)
        XCTAssertEqual(decodedJson?.pageSize, 20)
        XCTAssertEqual(decodedJson?.nextToken, "1561614900000-16387407846-N")
        XCTAssertEqual(decodedJson?.totalTransactions, 79)
        XCTAssertEqual(decodedJson?.transactionFeedStatus, "DELAYED")
    }

    func test_items_has_correct_values() throws {
        let item = decodedJson?.items?.first
        XCTAssertEqual(item?.transactionDate, ISO8601DateFormatter().date(from: "2010-01-01T12:00:00+0000"))
        XCTAssertEqual(item?.processedDate, ISO8601DateFormatter().date(from: "2010-01-01T12:00:00+0000"))
        XCTAssertEqual(item?.transactionId, "71280789078")
        XCTAssertEqual(item?.headerText, "Test Header")
        XCTAssertEqual(item?.descriptionText, "Description label")
        XCTAssertEqual(item?.points, "+1")
        XCTAssertEqual(item?.monetaryValue, "1.6")
        //TODO test some more BONUS / nil
        XCTAssertEqual(item?.displayType, displayType.normal)
        XCTAssertEqual(item?.detailTexts, ["Nice one, you got tests!"])

        let lineItems = item?.lineItems?.first
        XCTAssertEqual(lineItems?.description, "Item 1")
        XCTAssertEqual(lineItems?.points, "1")
        XCTAssertEqual(lineItems?.artworkUrl, "https://dmmids2yas2hi.cloudfront.net/sainsburys/nbm/Juno_93/7736185_5022313335117.png")
    }
}
